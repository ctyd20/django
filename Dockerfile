# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /composeexample
COPY requirements.txt /composeexample/
RUN pip install -r requirements.txt
COPY . /composeexample/

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]